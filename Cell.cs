namespace tictactoegame;

public struct Cell
{
    public readonly byte X, Y;
    public TicTacToe.Symbols Symbol = TicTacToe.Symbols.Empty;

    public Cell(byte x, byte y)
    {
        X = x;
        Y = y;
    }
}