﻿using System;

namespace tictactoegame;

internal class Program
{
    private static void Main()
    {
        // random generator for coordinates of the first moves
        var rnd = new Random();

        // initialize the board
        TicTacToe ticTacToe = new();

        // visualise the board in the console
        ticTacToe.DrawBoard();

        var currentPlayer = TicTacToe.Symbols.X;

        // make a first move as X, it's always a random one
        ticTacToe.MakeAMove(((byte)rnd.Next(0, 3), (byte)rnd.Next(0, 3)), currentPlayer);
        
        // visualise the board in the console
        ticTacToe.DrawBoard();

        // each player makes a move until game is over
        while (!ticTacToe.IsGameOver())
        {
            // switch players
            currentPlayer = currentPlayer == TicTacToe.Symbols.X ? TicTacToe.Symbols.O : TicTacToe.Symbols.X;

            // current player makes a move until game is over (win combo or full board)
            ticTacToe.MakeOptimalMove(currentPlayer);
            
            // visualise the board in the console
            ticTacToe.DrawBoard();
        }
    }
}