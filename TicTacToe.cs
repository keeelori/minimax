using System;
using System.Collections.Generic;

namespace tictactoegame;

/// <summary>
///     Defines the Tic Tac Toe.
/// </summary>
public class TicTacToe
{
    /// <summary>
    ///     Allowed set of symbols to fill the cells with.
    /// </summary>
    public enum Symbols : byte
    {
        Empty,
        X,
        O
    }

    /// <summary>
    ///     Represents the cells combinations which are considered as a win condition.
    /// </summary>
    public static readonly List<List<(byte X, byte Y)>> WinConditionCooridnates = new()
    {
        // columns
        new List<(byte X, byte Y)> { (0, 0), (0, 1), (0, 2) }, // first row
        new List<(byte X, byte Y)> { (1, 0), (1, 1), (1, 2) }, // second row
        new List<(byte X, byte Y)> { (2, 0), (2, 1), (2, 2) }, // second row

        // rows
        new List<(byte X, byte Y)> { (0, 0), (1, 0), (2, 0) }, // first column
        new List<(byte X, byte Y)> { (0, 1), (1, 1), (2, 1) }, // second column
        new List<(byte X, byte Y)> { (0, 2), (1, 2), (2, 2) }, // second column

        // diagonals
        new List<(byte X, byte Y)> { (0, 0), (1, 1), (2, 2) }, // first diagonal
        new List<(byte X, byte Y)> { (2, 0), (1, 1), (0, 2) } // second diagonal
    };

    /// <summary>
    ///     Stores the coordinates of the best move for the current player.
    /// </summary>
    private (byte col, byte row) _bestMoveCoordinates;

    /// <summary>
    ///     Represents the game Board as a 2-dimensional array of Cells.
    /// </summary>
    public Cell[,] Board { get; } =
    {
        { new(0, 0), new(0, 1), new(0, 2) },
        { new(1, 0), new(1, 1), new(1, 2) },
        { new(2, 0), new(2, 1), new(2, 2) }
    };

    /// <summary>
    ///     Sets the X or O into the specified cell.
    /// </summary>
    /// <param name="coordinates">x and y coordinates of a cell</param>
    /// <param name="symbol">the symbol to set</param>
    public void MakeAMove((byte X, byte Y) coordinates, Symbols symbol)
    {
        // check if coordinates are out of the Board space
        if (coordinates.X > 2 || coordinates.Y > 2)
            throw new ArgumentOutOfRangeException("Coordinates are out of bounds.");

        // check if the Cell is already occupied
        if (Board[coordinates.X, coordinates.Y].Symbol !=
            Symbols.Empty) // assuming Symbols.Empty is the default value for an empty cell
            throw new InvalidOperationException("Cell is already occupied.");

        // make a move
        Board[coordinates.X, coordinates.Y].Symbol = symbol;
    }

    /// <summary>
    ///     Returns true if a game is over, false if not.
    /// </summary>
    public bool IsGameOver()
    {
        return IsBoardFull() || DoesBoardHaveWinCombination().Item1;
    }

    /// <summary>
    ///     Returns true if Board is filled with symbols and false if there is at least 1 empty cell.
    /// </summary>
    private bool IsBoardFull()
    {
        for (var col = 0; col < Board.GetLength(0); col++)
        for (var row = 0; row < Board.GetLength(1); row++)
            if (Board[col, row].Symbol == Symbols.Empty)
                return false;

        return true;
    }

    /// <summary>
    ///     Returns true + winner's Symbol if there is a win combination on the Board.
    /// </summary>
    private (bool, Symbols) DoesBoardHaveWinCombination()
    {
        foreach (var winCombos in WinConditionCooridnates)

            // check that a specific win combo (column, row or diagonal) contains same symbols and symbol is not "Empty"
            if (Board[winCombos[0].X, winCombos[0].Y].Symbol == Board[winCombos[1].X, winCombos[1].Y].Symbol &&
                Board[winCombos[1].X, winCombos[1].Y].Symbol == Board[winCombos[2].X, winCombos[2].Y].Symbol &&
                Board[winCombos[0].X, winCombos[0].Y].Symbol != Symbols.Empty)
                return (true, Board[winCombos[0].X, winCombos[0].Y].Symbol);

        return (false, Symbols.Empty);
    }

    /// <summary>
    ///     Draws the Board.
    /// </summary>
    public void DrawBoard()
    {
        for (var i = 0; i < Board.GetLength(0); i++) // iterate through rows
        {
            for (var j = 0; j < Board.GetLength(1); j++) // iterate through columns
                if (Board[i, j].Symbol == Symbols.Empty)
                    Console.Write($"{'.',4}"); // draw "-" for Empty cell
                else
                    Console.Write($"{Board[i, j].Symbol,4}"); // draw X or O

            // new line for each row
            Console.WriteLine();
        }

        Console.WriteLine("-------------------------");
    }

    /// <summary>
    ///     Makes optimal move for the current player.
    /// </summary>
    public void MakeOptimalMove(Symbols symbol)
    {
        _bestMoveCoordinates = (0, 0);

        // assuming that the first move has been done already
        // (as it's not possible to calculate optimal move on an empty board)
        Minimax(symbol == Symbols.X ? true : false);

        MakeAMove(_bestMoveCoordinates, symbol);
    }

    /// <summary>
    ///     Returns the highest score for Maximization player and Lowest score for Minimization player.
    /// </summary>
    private int Minimax(bool isMaximizing)
    {
        // check if there is a winner
        (bool gameStatus, Symbols winner) gameOutcome = DoesBoardHaveWinCombination();

        // return score 1 is winner is X (maximizer) or -1 if O (minimizer)
        if (gameOutcome.gameStatus)
        {
            if (gameOutcome.winner == Symbols.X)
                return 1;
            return -1;
        }

        // check if tie (nobody wins)
        if (IsBoardFull() && !DoesBoardHaveWinCombination().Item1) return 0;

        if (isMaximizing)
        {
            var bestScore = int.MinValue;
            int score;

            for (var col = 0; col < Board.GetLength(0); col++)
            for (var row = 0; row < Board.GetLength(1); row++)
                if (Board[col, row].Symbol == Symbols.Empty)
                {
                    /* sorry for this code, it should be done properly, not hardcoded, but here i assume
                     that Maximizing player always plays X */
                    Board[col, row].Symbol = Symbols.X;

                    score = Minimax(false);
                    Board[col, row].Symbol = Symbols.Empty;

                    if (score > bestScore)
                    {
                        bestScore = score;
                        _bestMoveCoordinates.col = (byte)col;
                        _bestMoveCoordinates.row = (byte)row;
                    }
                }

            return bestScore;
        }
        else
        {
            var bestScore = int.MaxValue;
            int score;

            for (var col = 0; col < Board.GetLength(0); col++)
            for (var row = 0; row < Board.GetLength(1); row++)
                if (Board[col, row].Symbol == Symbols.Empty)
                {
                    /* sorry for this code, it should be done properly, not hardcoded, but here i assume
                     that Minimizing player always plays O */
                    Board[col, row].Symbol = Symbols.O;

                    score = Minimax(true);
                    Board[col, row].Symbol = Symbols.Empty;

                    if (score < bestScore)
                    {
                        bestScore = score;
                        _bestMoveCoordinates.col = (byte)col;
                        _bestMoveCoordinates.row = (byte)row;
                    }
                }

            return bestScore;
        }
    }
}